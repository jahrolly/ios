		//
//  ViewController.swift
//  Lab01
//
//  Created by Użytkownik Gość on 08.12.2017.
//  Copyright © 2017 Użytkownik Gość. All rights reserved.
//

import UIKit
        
class UtilityViewController: UIViewController {
    @IBOutlet weak var noOfRecords: UITextField!
    @IBOutlet weak var excutionTime: UITextView!
    @IBOutlet weak var output: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DAO.sharedInstance.createSensorTable()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func logExcecutionTime(excecutionTime : Double) {
        excutionTime.text = String(excecutionTime) + " [s]"
    }
    
    @IBAction func deleteAllRecords(_ sender: Any) {
        let startTime = NSDate()
        DAO.sharedInstance.deleteSensorValues()
        let finishTime = NSDate()
        let measuredTime = finishTime.timeIntervalSince(startTime as Date)
        logExcecutionTime(excecutionTime: measuredTime)
        output.text = ""
    }
    
    @IBAction func findExtremalValues(_ sender: Any) {
        let startTime = NSDate()
        let minTimestampSensor = DAO.sharedInstance.queryMinTimestamp()
        let maxTimestampSensor = DAO.sharedInstance.queryMaxTimestamp()
        let finishTime = NSDate()
        let measuredTime = finishTime.timeIntervalSince(startTime as Date)
        logExcecutionTime(excecutionTime: measuredTime)
        var outputString : String = ""
        outputString += "Lowest timestamp: " + String(minTimestampSensor.timestamp) + "\n"
        outputString += "Highest timestamp: " + String(maxTimestampSensor.timestamp) + "\n"
        output.text = outputString
    }
    
    @IBAction func findAverage(_ sender: Any) {
        let startTime = NSDate()
        let average = DAO.sharedInstance.queryAvg()
        let finishTime = NSDate()
        let measuredTime = finishTime.timeIntervalSince(startTime as Date)
        logExcecutionTime(excecutionTime: measuredTime)
        var outputString : String = ""
        outputString += "Average value: " + String(average) + "\n"
        output.text = outputString
    }
    
    @IBAction func findAveragePerSensor(_ sender: Any) {
        let startTime = NSDate()
        var outputString : String = DAO.sharedInstance.queryAvgValuePerSensor()
        let finishTime = NSDate()
        let measuredTime = finishTime.timeIntervalSince(startTime as Date)
        logExcecutionTime(excecutionTime: measuredTime)

        output.text = outputString
    }
    
    
    @IBAction func generateAndAddToDb(_ sender: Any) {
        let sensors = DAO.sharedInstance.querySensors()
        var sensorValues : [SensorValue] = []
        let noOfRecordsString = noOfRecords.text
        let noToGenerate = Int(noOfRecordsString ?? "1")!
        
        for _ in 1...noToGenerate {
            let randomIndex = Int(arc4random_uniform(UInt32(sensors.count)))
            let randomSensorName = sensors[randomIndex].name
            let randomValue = Double(arc4random()) / Double(UInt32.max) * 100
            let yearsSeconds = UInt32(31556926)
            let randomTimeStamp = Int(arc4random_uniform(yearsSeconds))
            sensorValues.append(SensorValue(timestamp : randomTimeStamp, sensorName : randomSensorName, value : randomValue))
        }
        let startTime = NSDate()
        DAO.sharedInstance.insertSensorsValues(sensorValues: sensorValues)
        let finishTime = NSDate()
        let measuredTime = finishTime.timeIntervalSince(startTime as Date)
        logExcecutionTime(excecutionTime: measuredTime)
        output.text = ""
    }
}
