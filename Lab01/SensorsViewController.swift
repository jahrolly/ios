//
//  ViewController.swift
//  Lab01
//
//  Created by Użytkownik Gość on 08.12.2017.
//  Copyright © 2017 Użytkownik Gość. All rights reserved.
//

import UIKit

class SensorsViewController: UITableViewController {
    let cellReuseIdentifier = "SensorTableViewCell"
    var sensors : [Sensor] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        sensors = DAO.sharedInstance.querySensors()
    }
    
    // number of rows in table view
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sensors.count
    }
    
    // create a cell for each table view row
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as? SensorTableViewCell else {
            fatalError("Dequed cell is not instance of sensorTableViewCell")
        }
        
        // set the text from the data model
        cell.name.text = self.sensors[indexPath.row].name
        cell.descLabel.text = self.sensors[indexPath.row].description
        
        return cell
    }
}
