//
//  SensorTableViewCell.swift
//  Lab01
//
//  Created by Karol on 29/12/2017.
//  Copyright © 2017 Użytkownik Gość. All rights reserved.
//

import UIKit

class SensorTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
