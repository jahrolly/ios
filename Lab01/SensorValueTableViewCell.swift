//
//  SensorValueTableViewCell.swift
//  Lab01
//
//  Created by Karol on 01/01/2018.
//  Copyright © 2018 Użytkownik Gość. All rights reserved.
//

import UIKit

class SensorValueTableViewCell: UITableViewCell {

    @IBOutlet weak var sensorName: UILabel!
    @IBOutlet weak var recordTimeStamp: UILabel!
    @IBOutlet weak var recordValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
