	//
//  SensorValue .swift
//  Lab01
//
//  Created by Karol on 29/12/2017.
//  Copyright © 2017 Użytkownik Gość. All rights reserved.
//

import UIKit

struct SensorValue {
    let timestamp : Int
    let sensorName : String
    let value : Double
}
