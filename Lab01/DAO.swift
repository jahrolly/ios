//
//  DAO.swift
//  Lab01
//
//  Created by Karol on 29/12/2017.
//  Copyright © 2017 Użytkownik Gość. All rights reserved.
//

import UIKit
import SQLite3

class DAO
{
    var db: OpaquePointer? = nil
    let dbPath : String = "data.db"
    let sensorTableCreateStatement : String = """
    CREATE TABLE IF NOT EXISTS Sensor(
    Name CHAR(25) PRIMARY KEY NOT NULL,
    Description CHAR(255));
    """
    let sensorValueTableCreateStatement : String = """
    CREATE TABLE IF NOT EXISTS SensorValue(
    Timestamp INTEGER NOT NULL,
    SensorName CHAR(25) NOT NULL,
    Value REAL NOT NULL,
    PRIMARY KEY (Timestamp, SensorName, Value));
    """
    
    let insertSensorString : String = "INSERT OR IGNORE INTO Sensor (Name, Description) VALUES (?, ?);"
    let insertSensorValueString : String = "INSERT INTO SensorValue (Timestamp, SensorName, Value) VALUES (?, ?, ?);"
    
    func createSensorTable() {
        createTable(statement: sensorTableCreateStatement)
        createTable(statement: sensorValueTableCreateStatement)
        insertSensors()
    }
    
    func createTable(statement : String) {
        var createTableStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, statement, -1, &createTableStatement, nil) == SQLITE_OK {
            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                print("Table created.")
            } else {
                print("Table could not be created.")
            }
        } else {
            print("CREATE TABLE statement could not be prepared.")
        }
        sqlite3_finalize(createTableStatement)
    }
    
    func insertSensors() {
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, insertSensorString, -1, &insertStatement, nil) == SQLITE_OK {
        for index in 1...20 {
            let stringIndex : String = String(index)
            let indexString : String = index <= 9 ? "S0" + stringIndex : "S" + stringIndex
            let descriptionString : String = "Sensor number " + stringIndex
            insertSensor(insertStatement : insertStatement, name: indexString as NSString, description: descriptionString as NSString)
        }
        sqlite3_finalize(insertStatement)
        }
        else {
            print("INSERT statement could not be prepared.")
        }
    }
    
    func querySensors() -> Array<Sensor> {
        let querySensorsString = "SELECT * FROM Sensor;"
        var queryStatement: OpaquePointer? = nil
        var sensors : [Sensor] = []
        if sqlite3_prepare_v2(db, querySensorsString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let name = sqlite3_column_text(queryStatement, 0)
                let description = sqlite3_column_text(queryStatement, 1)
                let nameString = String(cString: name!)
                let descriptionString = String(cString: description!)
                sensors.append(Sensor(name: nameString, description: descriptionString))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return sensors
    }
    
    func queryMaxTimestamp() -> SensorValue {
        let querySensorsString = "SELECT * FROM SensorValue ORDER BY Timestamp DESC LIMIT 1;"
        var queryStatement: OpaquePointer? = nil
        var sensorValue : SensorValue = SensorValue(timestamp: 0, sensorName : "", value : 0)
        if sqlite3_prepare_v2(db, querySensorsString, -1, &queryStatement, nil) == SQLITE_OK {
            if sqlite3_step(queryStatement) == SQLITE_ROW {
                let timestamp = sqlite3_column_int(queryStatement, 0)
                let sensorName = sqlite3_column_text(queryStatement, 1)
                let recordValue = sqlite3_column_double(queryStatement, 2)
                let sensorNameString = String(cString: sensorName!)
                sensorValue = SensorValue(timestamp: Int(timestamp), sensorName: sensorNameString, value: recordValue)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return sensorValue
    }
    
    func queryMinTimestamp() -> SensorValue {
        let querySensorsString = "SELECT * FROM SensorValue ORDER BY Timestamp ASC LIMIT 1;"
        var queryStatement: OpaquePointer? = nil
        var sensorValue : SensorValue = SensorValue(timestamp: 0, sensorName : "", value : 0)
        if sqlite3_prepare_v2(db, querySensorsString, -1, &queryStatement, nil) == SQLITE_OK {
            if sqlite3_step(queryStatement) == SQLITE_ROW {
                let timestamp = sqlite3_column_int(queryStatement, 0)
                let sensorName = sqlite3_column_text(queryStatement, 1)
                let recordValue = sqlite3_column_double(queryStatement, 2)
                let sensorNameString = String(cString: sensorName!)
                sensorValue = SensorValue(timestamp: Int(timestamp), sensorName: sensorNameString, value: recordValue)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return sensorValue
    }
    
    func queryAvg() -> Double {
        let querySensorsString = "SELECT avg(Value) FROM SensorValue;"
        var queryStatement: OpaquePointer? = nil
        var average : Double = 0
        if sqlite3_prepare_v2(db, querySensorsString, -1, &queryStatement, nil) == SQLITE_OK {
            if sqlite3_step(queryStatement) == SQLITE_ROW {
                average = sqlite3_column_double(queryStatement, 0)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return average
    }
    
    func recordsCount() -> Int {
        let querySensorsString = "select count(*) from SensorValue;"
        var queryStatement: OpaquePointer? = nil
        var count : Int = 0
        if sqlite3_prepare_v2(db, querySensorsString, -1, &queryStatement, nil) == SQLITE_OK {
            if sqlite3_step(queryStatement) == SQLITE_ROW {
                count = Int(sqlite3_column_int(queryStatement, 0))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return count
    }
    
    func queryAvgValuePerSensor() -> String {
        let querySensorsString = "select distinct(sensorName) as name, count(*), avg(Value) as average from SensorValue group by name;"
        var queryStatement: OpaquePointer? = nil
        var responseString : String = ""
        if sqlite3_prepare_v2(db, querySensorsString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let sensorName = sqlite3_column_text(queryStatement, 0)
                let count = Int(sqlite3_column_int(queryStatement, 1))
                let recordValue = sqlite3_column_double(queryStatement, 2)
                responseString += "Id: \(sensorName!), Cnt: \(count), Avg: \(recordValue)\n"
                }

        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return responseString
    }
    
    func querySensorValues() -> Array<SensorValue> {
        let querySensorsString = "SELECT * FROM SensorValue;"
        var queryStatement: OpaquePointer? = nil
        var sensorValues : [SensorValue] = []
        if sqlite3_prepare_v2(db, querySensorsString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let timestamp = sqlite3_column_int(queryStatement, 0)
                let sensorName = sqlite3_column_text(queryStatement, 1)
                let recordValue = sqlite3_column_double(queryStatement, 2)
                let sensorNameString = String(cString: sensorName!)
                sensorValues.append(SensorValue(timestamp: Int(timestamp), sensorName: sensorNameString, value: recordValue))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return sensorValues
    }
    
    func insertSensorsValues(sensorValues : [SensorValue]) {
        var insertStatement: OpaquePointer? = nil
        sqlite3_exec(db, "BEGIN EXCLUSIVE TRANSACTION", nil, nil, nil);
        if sqlite3_prepare_v2(db, insertSensorValueString, -1, &insertStatement, nil) == SQLITE_OK {
            for sensorValue in sensorValues {
                sqlite3_bind_int(insertStatement, 1, Int32(sensorValue.timestamp))
                let sensorName : NSString = sensorValue.sensorName as NSString
                sqlite3_bind_text(insertStatement, 2, sensorName.utf8String, -1, nil)
                sqlite3_bind_double(insertStatement, 3, sensorValue.value)
                if sqlite3_step(insertStatement) != SQLITE_DONE {
                    print("Could not insert record.")
                }
                sqlite3_reset(insertStatement)
            }
            
            sqlite3_finalize(insertStatement)
            if (sqlite3_exec(db, "COMMIT TRANSACTION", nil, nil, nil) != SQLITE_OK) {
                print("Error commiting transaction")
            }
        }
        else {
            print("INSERT statement could not be prepared.")
        }
        NotificationCenter.default.post(name: Notification.Name("SensorValuesChanged"), object: nil)
    }
    
    func deleteSensorValues() {
        var deleteStatement: OpaquePointer? = nil
        let deleteStatementString = "DELETE FROM SensorValue;"
        if sqlite3_prepare_v2(db, deleteStatementString, -1, &deleteStatement, nil) == SQLITE_OK {
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                print("Successfully deleted row.")
            } else {
                print("Could not delete row.")
            }
        } else {
            print("DELETE statement could not be prepared")
        }
        sqlite3_finalize(deleteStatement)
        NotificationCenter.default.post(name: Notification.Name("SensorValuesChanged"), object: nil)
    }
    
    func insertSensor(insertStatement: OpaquePointer?, name : NSString, description : NSString) {
            sqlite3_bind_text(insertStatement, 1, name.utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 2, description.utf8String, -1, nil)
            if sqlite3_step(insertStatement) != SQLITE_DONE {
                print("Could not insert row.")
            }
        sqlite3_reset(insertStatement)
    }
    
    
    init() {
        let dirPaths =  NSSearchPathForDirectoriesInDomains  (.documentDirectory, .userDomainMask, true)
        let docsDir : String = dirPaths.first ?? ""
        let fullPath = docsDir + "/" + dbPath
        if sqlite3_open(fullPath, &db) == SQLITE_OK {
            print("Successfully opened connection to database at \(fullPath)")
        } else {
            print("Unable to open database. Verify that you created the directory described " +
                "in the Getting Started section.")
        }
    }
    
    static let sharedInstance = DAO()
}
