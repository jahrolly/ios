//
//  ViewController.swift
//  Lab01
//
//  Created by Użytkownik Gość on 08.12.2017.
//  Copyright © 2017 Użytkownik Gość. All rights reserved.
//

import UIKit

class ReadingValuesViewController: UITableViewController {
    let cellReuseIdentifier = "SensorValueTableViewCell"
    var sensorValues : [SensorValue] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sensorValues = DAO.sharedInstance.querySensorValues()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("SensorValuesChanged"), object: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // number of rows in table view
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sensorValues.count
    }
    
    @objc func methodOfReceivedNotification(notification: Notification){
        sensorValues = DAO.sharedInstance.querySensorValues()
        self.tableView.reloadData()
    }
    
    // create a cell for each table view row
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as? SensorValueTableViewCell else {
            fatalError("Dequed cell is not instance of sensorTableViewCell")
        }
        

        // set the text from the data model
        cell.sensorName.text = self.sensorValues[indexPath.row].sensorName
        cell.recordTimeStamp.text = String(self.sensorValues[indexPath.row].timestamp)
        cell.recordValue.text = String(self.sensorValues[indexPath.row].value)
        
        return cell
    }
    
    
    
    
}
